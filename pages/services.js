import Head from "next/head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import styles from "../styles/Home.module.css";
import Services from "../components/Services";
import axios from "axios";
import {config} from "../config";
import {readFileSync} from "fs";
import {join} from "path";

export default function ServicesPage({servicesList, admin}) {
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={admin}/>

            <main className={styles.main}>
                <Services servicesList={servicesList}/>
            </main>

            <Footer/>
        </div>
    )
}

export async function getServerSideProps({req}) {
    const servicesList = await axios.get(config.API_URL + 'services')

    return {
        props: {servicesList: servicesList.data, admin: req.cookies.auth === readFileSync(join("auth.txt")).toString()}, // will be passed to the page component as props
    }
}
