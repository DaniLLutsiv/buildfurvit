import Head from "next/head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import styles from "../styles/Home.module.css";
import Contact from "../components/Contact/Contact";
import {readFileSync} from "fs";
import {join} from "path";

export default function ContactPage({admin}) {
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={admin}/>

            <main className={styles.main}>
                <Contact/>
            </main>

            <Footer/>
        </div>
    )
}

export async function getServerSideProps({req}) {

    return {
        props: {admin: req.cookies.auth === readFileSync(join("auth.txt")).toString()}, // will be passed to the page component as props
    }
}