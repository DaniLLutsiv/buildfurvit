import styles from "../styles/Home.module.css";
import Head from "next/head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import axios from "axios";
import {config} from "../config";
import Admin from "../components/Admin";

const fs = require('fs')
const path = require('path')

const Reviews = ({services}) => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={true}/>

            <main className={styles.main}>
                <Admin services={services} />
            </main>

            <Footer/>
        </div>
    )
}

export default Reviews


export async function getServerSideProps({req}) {
    if (req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString()){
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }

    try {
        const services = await axios.get(config.API_URL + 'services')

        return {
            props: {services: services.data}, // will be passed to the page component as props
        }
    } catch (e) {
        return {
            props: {services: []}, // will be passed to the page component as props
        }
    }
}