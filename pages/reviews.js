import styles from "../styles/Home.module.css";
import Head from "next/head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import ReviewsList from "../components/Reviews/ReviewsList";
import axios from "axios";
import {config} from "../config";
import {readFileSync} from "fs";
import {join} from "path";
import {useState} from "react";

const Reviews = ({list, admin}) => {
    const [localList, setList] = useState(list)
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={admin}/>

            <main className={styles.main} style={{marginTop: 0}}>
                <ReviewsList list={localList} admin={admin} setList={setList}/>
            </main>

            <Footer/>
        </div>
    )
}

export default Reviews


export async function getServerSideProps({req}) {
    const list = await axios.get(config.API_URL + 'reviews')


    return {
        props: {list: list.data, admin: req.cookies.auth === readFileSync(join("auth.txt")).toString()}, // will be passed to the page component as props
    }
}