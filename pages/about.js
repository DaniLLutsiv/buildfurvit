import Head from "next/head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import styles from "../styles/Home.module.css";
import AboutContent from "../components/About";

export default function About({admin}) {
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={admin}/>

            <main className={styles.main}>
                <AboutContent/>
            </main>

            <Footer/>
        </div>
    )
}
