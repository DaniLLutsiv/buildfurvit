import styles from '../styles/Home.module.css'
import Header from "../components/Header";
import HeaderMain from "../components/Home/Header";
import Slider from "../components/Home/Slider";
import Info from "../components/Home/Info";
import Services from "../components/Home/Services";
import Form from "../components/Home/Form";
import Footer from "../components/Footer";
import {useRef} from "react";
import axios from "axios";
import {config} from "../config";
import {readFileSync} from "fs";
import {join} from "path";
import {badRooms} from "../components/About";

export default function Home({servicesList, admin}) {
    const ref = useRef()
    const inputRef = useRef()

    return (
        <div className={styles.container}>
            <Header admin={admin}/>

            <main className={styles.main}>
                <HeaderMain refForm={ref}/>
                <Slider images={badRooms} autoPlay/>
                <Info/>
                <Services servicesList={servicesList}/>
                <Form refForm={ref} inputRef={inputRef}/>
            </main>

            <Footer refForm={ref} inputRef={inputRef}/>
        </div>
    )
}

export async function getServerSideProps({req}) {
    const servicesList = await axios.get(config.API_URL + 'services')

    return {
        props: {servicesList: servicesList.data, admin: req.cookies.auth === readFileSync(join("auth.txt")).toString()}, // will be passed to the page component as props
    }
}