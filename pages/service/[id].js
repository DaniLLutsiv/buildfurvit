import {readFileSync} from "fs";
import {join} from "path";
import styles from "../../styles/Home.module.css";
import Head from "next/head";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Service from "../../components/Service";

const ServicePage = ({id, img, name, price, list, admin, description}) => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Качественое и быстрое строительство в Запорожье</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <Header admin={admin}/>

            <main className={styles.main}>
                <Service id={id} img={img} name={name} price={price} list={list} description={description}/>
            </main>

            <Footer/>
        </div>
    )
}

export default ServicePage


export async function getServerSideProps({req, params}) {
    const data = JSON.parse(readFileSync(join("сервисы.txt")).toString()).find(el => el.id == params.id)

    return {
        props: {...data, admin: req.cookies.auth === readFileSync(join("auth.txt")).toString()}, // will be passed to the page component as props
    }
}