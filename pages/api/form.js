const nodemailer = require("nodemailer");
const fs = require('fs')
const path = require('path')
const axios = require('axios')

function main() {
    return nodemailer.createTransport({
        service: "Gmail",
        auth: {
            user: 'danya300914@gmail.com', // generated ethereal user
            pass: 'b348d467c', // generated ethereal password
        },
    });
}

export default async (req, res) => {
    if (req.method === 'POST') {
        if (req.body.name?.length > 1 && req.body.phone.length > 5){
            const time = new Date( Date.now() + 1000*60*60*3).toISOString().replace(/T/, ' ').replace(/\..+/, '')

            let subServices = ''

            req.body?.list?.forEach(el => {
                subServices += ` заказаная услуга: ${el.name}, цена: ${el.price}, id: ${el.id};`
            })

            try {
                fs.appendFileSync(path.join("заказы.txt"), `Имя: ${req.body.name}, Телефон: ${req.body.phone}, время заказа: ${time}, ${req.body.data ? ('номер заказаной позиции: ' + req.body.data.id + ',' + ' имя заказаной позиции: ' + req.body.data.name) : ''} ${subServices ? ', подуслуги:' + subServices : ''} \n`);

                const transporter = main()

                await transporter.sendMail({
                    from: 'Build FurVit', // sender address
                    to: "danya300914@gmail.com", // list of receivers
                    subject: `Заказ Build FurVit, время заказа: ${time}`,
                    text: `Имя: ${req.body.name}, Телефон: ${req.body.phone}, время заказа: ${time}, ${req.body.data ? ('номер заказаной позиции: ' + req.body.data.id + ',' + ' имя заказаной позиции: ' + req.body.data.name) : ''} ${subServices ? ', подуслуги:' + subServices : ''} \n`, // plain text body
                });
            } catch (e) {
                console.log(e)
                res.status(400).json({error: true, name: 'mailer error'})
                return;
            }

            try {
                await axios.post('https://api.telegram.org/bot1642572678:AAF-VKQmjtk9OmkHXQCzzK-IWd4-4BdoMFU/sendMessage', {
                    text: `Заказ Build FurVit, Имя: ${req.body.name}, Телефон: ${req.body.phone}, время заказа: ${time}, ${req.body.data ? ('номер заказаной позиции: ' + req.body.data.id + ',' + ' имя заказаной позиции: ' + req.body.data.name) : ''} ${subServices ? ', подуслуги:' + subServices : ''}`,
                    chat_id: 787510386,
                })
                res.status(200).json({success: true})
            } catch (e) {
                res.status(400).json({error: true, name: 'telegram error'})
            }
        }else{
            res.status(400).json({error: true, name: 'incorrect data'})
        }
    } else {
        res.status(404).end()
    }
}
