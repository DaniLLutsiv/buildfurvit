const fs = require('fs')
const path = require('path')
const crypto = require('crypto')

export default async function handler(req, res) {
    if (req.method === 'GET') {
        try {
            const data = JSON.parse(fs.readFileSync(path.join("сервисы.txt")).toString())
            res.status(200).json(data);
        } catch (e) {
            console.log(e);
            res.status(400).json([])
        }
    } else if (req.method === 'PUT') {
        if ((req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString())) {
            res.status(401).send([]);
            return;
        }

        const data = JSON.parse(fs.readFileSync(path.join("сервисы.txt")).toString())
        const body = req.body

        if (req.body.newImg) {
            const hash = crypto.createHash('sha256').update(Date.now().toString()).digest('hex');
            const buf = Buffer.from(req.body.newImg.replace(/^data:image\/\w+;base64,/, ""), 'base64');
            fs.writeFileSync(path.join(`public`, `images`, `services`, `${hash}.jpg`), buf)

            try {
                fs.unlinkSync(path.join(`public`, `${req.body.img}`))
            } catch (e) {
                console.log(e)
            }

            req.body.img = '/images/services/' + hash + '.jpg'
        }

        const newData = data.map(el => el.id === body.id ? {
            id: body.id,
            name: body.name,
            img: body.img,
            price: body.price,
            list: req.body.list,
            description: req.body.description,
        } : el)

        fs.writeFileSync('сервисы.txt', JSON.stringify(newData))
        res.status(200).json(newData);

    } else if (req.method === 'DELETE') {
        if ((req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString())) {
            res.status(401).send([]);
            return;
        }

        const data = JSON.parse(fs.readFileSync(path.join("сервисы.txt")).toString())
        const editData = req.body
        const newData = data.filter(el => el.id !== editData.id)

        try {
            fs.unlinkSync(path.join(`public`, `${req.body.img}`))
        } catch (e) {
        }

        fs.writeFileSync('сервисы.txt', JSON.stringify(newData))
        res.status(200).json(newData);

    } else if (req.method === 'POST') {
        if ((req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString())) {
            res.status(401).send([]);
            return;
        }

        const data = JSON.parse(fs.readFileSync(path.join("сервисы.txt")).toString())
        const hash = crypto.createHash('sha256').update(Date.now().toString()).digest('hex');

        data.push({
            id: Date.now(),
            name: req.body.name,
            img: '/images/services/' + hash + '.jpg',
            price: req.body.price,
            list: req.body.list,
            description: req.body.description,
        })

        const buf = Buffer.from(req.body.img.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        fs.writeFileSync(path.join(`public`, `images`, `services`, `${hash}.jpg`), buf)

        fs.writeFileSync('сервисы.txt', JSON.stringify(data))
        res.status(200).json(data);
    }
}