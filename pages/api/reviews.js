const fs = require('fs')
const path = require('path')

export default async function handler(req, res) {
    if (req.method === 'GET') {
        try {
            const data = JSON.parse(fs.readFileSync(path.join("отзывы.txt")).toString())
            res.status(200).json(data);
        } catch (e) {
            res.status(400).send([]);
        }
    } else if (req.method === 'PUT'){
        if ((req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString())){
            res.status(401).send([]);
            return;
        }

        const data = JSON.parse(fs.readFileSync(path.join("отзывы.txt")).toString())
        const body = req.body
        const newData = data.map(el => el.id === body.id ? body : el)
        fs.writeFileSync('отзывы.txt', JSON.stringify(newData))
        res.status(200).json(newData);

    } else if (req.method === 'DELETE') {
        if ((req.cookies.auth !== fs.readFileSync(path.join("auth.txt")).toString())){
            res.status(401).send([]);
            return;
        }

        const data = JSON.parse(fs.readFileSync(path.join("отзывы.txt")).toString())
        const editData = req.body
        const newData = data.filter(el => el.id !== editData.id)

        fs.writeFileSync('отзывы.txt', JSON.stringify(newData))
        res.status(200).json(newData);

    } else if (req.method === 'POST') {
        req.body.id = Date.now()
        const data = JSON.parse(fs.readFileSync(path.join("отзывы.txt")).toString())

        const editData = req.body
        data.push(editData)

        fs.writeFileSync('отзывы.txt', JSON.stringify(data))
        res.status(200).json(data);
    }
}


