import '../styles/globals.css'
import '../styles/image-gallery.css'
import NextNprogress from 'nextjs-progressbar';
import {TransitionUp, useStyles} from "../components/Modal";
import {Snackbar} from "@material-ui/core";
import {createContext, useState} from "react";
import Head from "next/head";
import {config} from "../config";

export const SnackbarContext = createContext('snackbar');

function MyApp({Component, pageProps}) {
    const [snackbar, setSnackbar] = useState(null)
    const classes = useStyles({status: snackbar?.status});

    return (
        <SnackbarContext.Provider value={{snackbar, setSnackbar}}>
            <Head>
                <title>Строительство в Запорожье | Качествено и быстро </title>
                <link rel="canonical" href={config.CLIENT_URL} />
                <link rel="icon" href="/favicon.ico"/>
                <link rel="apple-touch-icon" href="/favicon.ico" />
                <meta httpEquiv="Content-Language" content="ru"/>
                <meta httpEquiv="Content-type" content="text/html;charset=windows-1251"/>
                <meta name="Address" content="г. Запорожье"/>
                <meta name="robots" content="index"/>
                <meta name="Copyright" content="Смиюха Виталий Степанович"/>
                <meta name="description" content="BuildFurVit - команда профессионалов своего дела. Мы работаем на Запорожском рынке ремонтно-строительных услуг более 15 лет." />
            </Head>

            <NextNprogress
                color="#ffffff"
                startPosition={0.3}
                stopDelayMs={200}
                height="2"
            />

            <Component {...pageProps} />

            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                open={!!snackbar}
                onClose={() => setSnackbar(false)}
                message={snackbar?.msg}
                key={'bottom' + 'right'}
                classes={{root: classes.snackbar}}
                TransitionComponent={TransitionUp}
            />
        </SnackbarContext.Provider>
    )
}

export default MyApp
