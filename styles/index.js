import styled from "styled-components";

export const button = styled.button`
  border: 0;
  outline: 0;
  background-color: #fff;
  cursor: pointer;
`