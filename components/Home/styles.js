import styled from "styled-components";
import {Flex} from "../Header/styles";
import {button} from "../../styles";

export const HeaderH1 = styled.h1`
  margin: 0;
  text-align: left;
  padding: 0 20px;
  width: min-content;
  min-width: 200px;

  @media (max-width: 768px) {
    text-align: center;
    font-size: 30px;
  }
`

export const HeaderMainWrapper = styled(Flex)`
  width: 100%;
  margin: 0 0 35px 0;
  padding: 0 20px;

  & > button {
    background-color: #fbc32b;
    color: #000;
  }

  & > button:hover {
    background-color: #eab628;
  }

  @media (max-width: 768px) {
    display: flex;
    flex-direction: column;
    margin: 35px 0;

    & > button {
      margin-top: 15px;
    }
  }
`

export const Text = styled.h2`
  text-transform: uppercase;
  font-size: 27px;
  font-weight: 700;
`

export const Yellow = styled.span`
  color: #eab628;
`

export const List = styled.div`
  display: flex;
  margin: 50px 0 30px;
  width: 100%;
  flex-wrap: wrap;

  @media (max-width: 500px) {
    margin-top: 0;
    padding: 10px;
  }
`

export const ListItem = styled(Flex)`
  width: 25%;
  flex-direction: column;

  @media (max-width: 480px) {
    width: 50%;
  }
`

export const ItemIcon = styled.div`
  text-align: center;
`

export const ItemText = styled.h3`
  text-align: center;
  margin: auto;
  font-weight: 400;
  font-size: 1rem;
  max-width: 200px;
`

export const ServicesWrapper = styled.div`
  width: 100%;
  height: 550px;
  overflow: hidden;
  position: relative;
`

export const ImageWrapper = styled.div`
  overflow: hidden;
  height: 100%;

  & > img {
    min-width: 100%;
  }
`

export const ServicesText = styled.h2`
  width: 100%;
  text-align: center;
  font-size: 40px;
  font-weight: 700;
  position: absolute;
  top: 20px;
  margin: 0;
`

export const ServicesList = styled.div`
  display: flex;
  margin: 50px 5% 30px;
  width: 90%;
  position: absolute;
  top: 50px;
  left: 0;
  max-height: 300px;
  justify-content: space-between;

  & > div > img {
    transition: all .3s;
    cursor: pointer;
  }

  & > div > img:hover {
    transform: scale(1.2)
  }
`

export const Service = styled.div`
  padding: 15px 10px;
  max-width: 269px;
  height: 370px;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  background-color: #fff;
  opacity: 0;
  transition: box-shadow 1s, opacity 1s;
  animation: ${({show}) => show ? 'show' : 'hide'} .3s forwards;
  box-shadow: 0 0 10px 4px #eae9e6;

  & > div {
    flex-grow: 1;
    width: 100%;
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: flex-end;

    & > div {
      width: 100%;
    }
  }

  & > div > img {
    height: 200px;
    transition: all .5s;
  }

  &:hover {
    box-shadow: 0 0 10px 4px #9f9c97;

    & > div > img {
      transform: scale(1.2);
    }
  }
  
  & a {
    width: 100%;
  }

  @keyframes show {
    from {
      opacity: 0;
    }

    to {
      opacity: 1;
    }
  }

  @keyframes hide {
    from {
      opacity: 1;
    }

    to {
      opacity: 0;
    }
  }
`

export const Image = styled.div`
  overflow: hidden;
  
  & img {
    object-fit: cover;
  }
`

export const ServiceName = styled.h3`
  text-align: center;
  font-weight: 700;
  margin: 17px 0 0 0;
  font-size: 1rem;
`

export const ServicePrice = styled.div`
  text-align: center;
  margin-top: 15px;
  color: #333;
`

export const ServiceButton = styled(button)`
  width: 50%;
  margin: 20px auto 0;
  background-color: #FBC01E;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  font-weight: 600;
  font-size: 14px;

  & > span {
    margin-bottom: 20px;
  }

  &:hover {
    background-color: #f3ba1d;
    text-decoration: none;
    cursor: pointer;
    box-shadow: 0 0 10px 2px #e0dede;
  }
`

export const FormWrapper = styled.section`
  width: 100%;
  padding: 50px;
  display: flex;

  @media (max-width: 768px) {
    flex-direction: column;

    & > div {
      width: 100%;
    }

    & > div:first-child {
      margin-bottom: 30px;
    }
  }

  @media (max-width: 480px){
    padding: 25px;
  }
`

export const FormText = styled.div`
  width: 50%;
  padding-right: 30px;
  margin-top: 20px;

  & > span {
    text-indent: 20px
  }

  @media (max-width: 500px) {
    padding-right: 0;
    margin-top: 10px;
    width: 100%;
  }
`

export const FormContainer = styled.div`
  width: 50%;
  background: #373B4B;
  padding: 60px 60px 70px;
  box-shadow: 0 5px 23px 0 rgb(0 0 0 / 3);
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media (max-width: 500px) {
    width: 100%;
    padding: 40px 40px 50px;
  }
`

export const FormHeader = styled.h3`
  color: #fff;
  font-weight: 400;
  font-size: 24px;
  margin: 0;

  @media (max-width: 768px) {
    font-size: 18px;
  }
`
export const InputWrapper = styled.div`
  margin-top: 25px;
  width: 100%;


  & > div > div > input {
    width: 100%;
    color: #fff;
  }

  & > div > div:before, & > div > div:after {
    border-color: #fff !important;
  }

  & > div > label {
    left: -15px;
    color: #fff !important;
  }

  & > div {
    width: 100%;
  }

  & > div > div {
    width: 100%;
  }

  & > div > div > div {
    width: 100%;
  }

`

export const FormButton = styled(ServiceButton)`
  position: relative;
  margin-left: 0;
  margin-top: 30px;
  max-width: 150px;
  background-color: ${({disable}) => disable ? '#a2873c' : '#FBC01E'};

  &:hover {
    cursor: ${({disable}) => disable ? 'auto !important' : 'pointer'};
    background-color: ${({disable}) => disable ? '#a2873c' : '#cda841'};
    box-shadow: ${({disable}) => disable ? 'none' : '0 0 10px 2px #323232'};
  }


  @media (max-width: 768px) and (min-width: 500px) {
    margin-right: 0;
    margin-left: auto;
  }
`

export const ListDescription = styled.div`
  margin-top: 20px;

  & > div:first-child {
    font-weight: 700;
    margin-bottom: 15px;
  }

  @media (max-width: 500px) {
    margin-top: 30px;
  }
`

export const DescriptionItem = styled.h4`
  margin: 5px 0 0 0;
  display: flex;
  font-weight: 400;
  font-size: 1rem;

  &:before {
    content: "";
    min-width: 7px;
    height: 7px;
    border-radius: 50%;
    margin-top: 10px;
    margin-right: 8px;
    background-color: #f3ba1d;
    display: block;
  }
`