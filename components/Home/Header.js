import {HeaderH1, HeaderMainWrapper, Text, Yellow} from "./styles";
import {Button} from "@material-ui/core";

const HeaderMain = ({refForm}) => {
    const handler = () => {
        refForm.current.scrollIntoView()
    }

    return (
        <>
            <HeaderMainWrapper>
                <HeaderH1>
                    СТРОИТЕЛЬСТВО <Yellow>ЗАПОРОЖЬЕ</Yellow>
                </HeaderH1>
                <Button variant="contained" onClick={handler}>Заказать обратный звонок</Button>
            </HeaderMainWrapper>
        </>
    )
}

export default HeaderMain
