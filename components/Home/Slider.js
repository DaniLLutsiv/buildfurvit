import React from "react";
import ImageGallery from 'react-image-gallery';
import ImageNext from 'next/image'

const Slider = ({images, autoPlay = false, className = "custom-gallery"}) => {
    return (
        <ImageGallery slideDuration={800} slideInterval={5000} items={images} autoPlay={autoPlay} showPlayButton={false}
                      showFullscreenButton={false} showThumbnails={false} lazyLoad additionalClass={className} renderItem={(el) => <ImageNext src={el.original} alt={el.originalAlt} layout="fill"/>
        }/>
    )
}

export default Slider