import {useMemo} from "react";
import {ItemIcon, ItemText, List, ListItem} from "./styles";
import ImageNext from 'next/image'

const itemList = [
    {
        img: '/images/clock.png',
        text: 'Мы всегда на связи с клиентами'
    },
    {
        img: '/images/otsrochka.png',
        text: 'Даем клиентам возможность отсрочки оплаты'
    },
    {
        img: '/images/garantiya.png',
        text: 'Даем гарантию на год на все виды работ'
    },
    {
        img: '/images/services.png',
        text: 'Принимаем заказы любой сложности'
    },
]

const Info = () => {
    const list = useMemo(() => (
        itemList.map(({img, text, custom}, i) => (
            <ListItem key={text + i}>
                <ItemIcon>
                    <ImageNext src={img} alt={text} width={92} height={92}/>
                </ItemIcon>
                <ItemText>
                    {text}
                </ItemText>
            </ListItem>
            )
        )
    )
    , [])

    return (
        <List>
            {list}
        </List>
    )
}

export default Info