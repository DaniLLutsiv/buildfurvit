import {ImageWrapper, ServicesList, ServicesText, ServicesWrapper} from "./styles";
import {useEffect, useState} from "react";
import useWindowSize from "../../hooks/useWindowSize";
import List from "../Services/List";
import ImageNext from 'next/image'

const Services = ({servicesList}) => {
    const [page, setPage] = useState(0)
    const size = useWindowSize();
    const [show, setShow] = useState(true)

    useEffect(() => {
        setPage(0)
    }, [size])

    const count = size.width < 1500 ? Math.floor(size.width / 400) : 3

    const currentList = servicesList.slice(page * count, ((page * count + (count)) || 1))

    const handler = (mode) => () => {
        if (mode === 'prev' && page <= 0) return;
        if (mode === 'next' && page < 0) return;

        setShow(false)
        setTimeout(() => {
            if (mode === 'prev') {
                setPage(prev => prev > 0 ? prev - 1 : 0)
            } else {
                setPage(prev => servicesList.length > (count * prev + (count)) ? prev + 1 : 0)
            }
            setShow(true)
        }, 300)
    }

    return (
        <ServicesWrapper>
            <ImageWrapper>
                <ImageNext src="/images/bg.jpg" alt="гранит" layout="fill"/>
            </ImageWrapper>
            <ServicesText>Наши услуги</ServicesText>
            <ServicesList>
                <ImageNext src="/images/arrow.svg" alt="<" onClick={handler('prev')} width={20} height={25} className="rotate"/>
                <List show={show} list={currentList}/>
                <ImageNext src="/images/arrow.svg" alt=">" onClick={handler('next')} width={20} height={25}/>
            </ServicesList>
        </ServicesWrapper>
    )
}

export default Services