import {
    DescriptionItem,
    FormButton,
    FormContainer,
    FormHeader,
    FormText,
    FormWrapper,
    InputWrapper,
    ListDescription
} from "./styles";
import {CircularProgress, FormControl, Input, InputLabel, Snackbar} from "@material-ui/core";
import {useEffect, useState} from "react";
import axios from "axios";
import {config} from "../../config";
import {TransitionUp, useStyles} from "../Modal";
import Link from 'next/link'

const Form = ({refForm, inputRef}) => {
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [disable, setDisable] = useState(true)
    const [loader, setLoader] = useState(false)
    const [snackbar, setSnackbar] = useState(null)
    const classes = useStyles({status: snackbar?.status});

    useEffect(() => {
        if (name.length > 1 && /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(phone)) {
            setDisable(false)
        } else {
            setDisable(true)
        }
    }, [name, phone])

    const submit = async () => {
        if (!disable) {
            setLoader(true)
            try {
                await axios.post(config.CLIENT_URL + 'form', {phone, name})
                setSnackbar({msg: 'Заказ успешно создан', status: 'success'})
            } catch (e) {
                const msg = e.message === 'Network Error' ? 'Проверьте подключение к сети' : 'Не удалось создать заказ, попробуйте позже'
                setSnackbar({msg, status: 'error'})
            } finally {
                setLoader(false)
                setTimeout(() => setSnackbar(null), 3000)
            }
        }
    }


    return (
        <FormWrapper ref={refForm}>
            <FormText>
                <span>
                    <p>Мы <b>«BuildFurVit»</b> - команда профессионалов своего дела. Мы работаем на Запорожском рынке ремонтно-строительных услуг более 15 лет. Благодаря безупречному качеству, умеренным ценам и доверия клиентов мы с лёгкостью и безупречностью справляемся с конкуренцией и являемся лидерами на рынке.</p>
                    <p>
                        <b>«BuildFurVit»</b> занимает одно из лидирующих мест на рынке ремонта Запорожья и Запорожской области! Также мы можем предоставлять услуги в других городах Украины!
                    </p>
                    <p>
                        Более детально с нашими услагими вы можете ознакомиться к разделе&nbsp;
                        <b>
                            <Link href="/services">
                                <a>Услуги</a>
                            </Link>
                        </b>
                    </p>
                </span>
                <ListDescription>
                    <div>Основные услуги, которые мы представляем:</div>
                    <DescriptionItem>
                        Демонтаж стен, полов и тд.
                    </DescriptionItem>
                    <DescriptionItem>
                        Монтаж любых конструкций
                    </DescriptionItem>
                    <DescriptionItem>
                        Электротехнические работы
                    </DescriptionItem>
                    <DescriptionItem>
                        Установка мебели
                    </DescriptionItem>
                </ListDescription>
            </FormText>
            <FormContainer>
                <FormHeader>
                    Мастера к вашим услугам!
                </FormHeader>
                <FormHeader>
                    Сделайте заказ прямо сейчас:
                </FormHeader>
                <InputWrapper>
                    <FormControl>
                        <InputLabel htmlFor="name">Фио</InputLabel>
                        <Input
                            inputRef={inputRef}
                            id="standard-adornment-password"
                            type="text"
                            placeholder="Иванов Иван Иванович"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                    </FormControl>
                </InputWrapper>
                <InputWrapper>
                    <FormControl>
                        <InputLabel htmlFor="phone">Номер телефона</InputLabel>
                        <Input
                            id="phone"
                            type="text"
                            placeholder="380123456789"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                        />
                    </FormControl>
                </InputWrapper>
                <FormButton disable={disable} onClick={submit}>
                    <p>Отправить</p>
                    {loader && (
                        <div className={classes.root}>
                            <CircularProgress/>
                        </div>
                    )}
                </FormButton>
            </FormContainer>
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                open={!!snackbar}
                message={snackbar?.msg}
                key={'bottom' + 'right'}
                classes={{root: classes.snackbar}}
                TransitionComponent={TransitionUp}
            />
        </FormWrapper>
    )
}

export default Form