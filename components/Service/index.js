import React, {useState} from 'react'
import {
    Action,
    Content,
    Description,
    Free,
    Image,
    MyTable,
    MyTableHead, ServiceButton33,
    ServiceContainer,
    ServiceWrapper,
    Slogo,
    Table,
    Text
} from "./styles";
import {HeaderH1} from "../Home/styles";
import {Checkbox, TableBody, TableCell, TableRow, withStyles} from "@material-ui/core";
import Modal from "../Modal";

const Service = ({id, img, list = [], name, price, text, description}) => {
    const [active, setActive] = useState(null)
    const [checked, setChecked] = useState({})

    const selectList = list.filter(el => Object.keys(checked).find(key => checked[key] && key === el.id.toString()))

    const checkHandler = (id) => {
        setChecked(prev => ({...prev, [id]: !prev[id]}))
    }

    return (
        <ServiceWrapper>
            <HeaderH1 style={{width: '100%'}}>
                {name}
            </HeaderH1>

            <ServiceContainer>
                <Image>
                    <img src={img} alt={name}/>
                    <Action>
                        <div>
                            <Slogo>
                                {name.toUpperCase()} - ДОВЕРЬТЕСЬ ПРОФЕССИОНАЛАМ!
                            </Slogo>
                            <Free>
                                * Бесплатная консультация
                            </Free>
                        </div>

                        <ServiceButton33 onClick={() => !isEmpty(checked) && setActive({
                            id,
                            img,
                            list: list.filter(el => checked[el.id]),
                            name,
                            price,
                            text
                        })} disable={isEmpty(checked)}>
                            <p>Заказать</p>
                        </ServiceButton33>
                    </Action>
                </Image>
                <Content>
                    {description && (
                        <div>
                            <Description>Описание</Description>
                            <Text>
                                {description}
                            </Text>
                        </div>
                    )}

                    {!!list.length && (
                        <MyTable>
                            <MyTableHead>
                                <TableRow>
                                    {columns.map((column) => (
                                        <TableCell
                                            key={column.id}
                                            style={{minWidth: column.minWidth || 'auto'}}
                                        >
                                            {column.label}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </MyTableHead>
                            <TableBody>
                                {list.map((row) => (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                                        <TableCell padding="checkbox" style={{maxWidth: '50px', width: 'initial'}}>
                                            <MyCheckbox
                                                onChange={() => checkHandler(row.id)}
                                                checked={!!checked[row.id]}
                                                style={{marginLeft: '15px'}}
                                                id={row.id.toString()}
                                            />
                                        </TableCell>
                                        <TableCell style={{maxWidth: '200px'}}>
                                            <label htmlFor={row.id.toString()}>
                                                {row.name}
                                            </label>
                                        </TableCell>
                                        <TableCell style={{paddingLeft: "31px", maxWidth: '150px'}}>
                                            <label htmlFor={row.id.toString()}>
                                                {row.units}
                                            </label>
                                        </TableCell>
                                        <TableCell>
                                            <label htmlFor={row.id.toString()}>
                                                {row.price} грн.
                                            </label>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </MyTable>
                    )}
                </Content>
            </ServiceContainer>

            <Modal open={active} setOpen={setActive} list={selectList}/>
        </ServiceWrapper>
    )
}

export default Service

const columns = [
    {id: '1', label: 'Выбрание услуги'},
    {id: '2', label: 'Имя услуги'},
    {id: '3', label: 'Ед. измерения', minWidth: '150px'},
    {id: '4', label: 'Цена', minWidth: '120px'},
];

const MyCheckbox = withStyles({
    root: {
        width: "40px",
        height: "40px",
    },
    icon: {
        width: "40px",
        height: "40px",
    }
})((props) => <Checkbox color="default" {...props} />);

function isEmpty(obj) {
    for (let key in obj) {
        return !obj[key]
    }
    return true;
}
