import styled from "styled-components";
import {button} from "../../styles";
import {Table, TableHead} from "@material-ui/core";

export const ServiceWrapper = styled.div`
  width: 100%;
  padding: 0 20px;
  margin-bottom: 40px;

  @media (max-width: 768px) {
    padding-top: 20px;
  }
`

export const ServiceContainer = styled.div`
  display: flex;
  padding: 20px;
  position: relative;

  @media (max-width: 900px) {
    flex-direction: column;
  }
`

export const Image = styled.div`
  width: 100%;
  margin-right: 20px;
  max-width: 500px;
  display: flex;
  position: sticky;
  top: 100px;
  left: 0;
  flex-direction: column;
  height: fit-content;

  & img {
    width: 100%;
    max-width: 100%;
    min-width: 100%;
    max-height: 100%;
    height: 100%;
    min-height: 100%;
  }

  @media (max-width: 900px) {
    position: static;
    max-width: 100% !important;
  }
`

export const Action = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 30px 20px;
  align-items: center;
  background-color: #313543;

  @media (max-width: 480px) {
    flex-direction: column;
    align-items: flex-start;
  }

  @media (min-width: 900px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

export const Slogo = styled.div`
  color: #fff;
  font-weight: 700;
  font-size: 16px;
  line-height: 1.4;
`

export const Free = styled(Slogo)`
  font-weight: 500;
  margin-top: 15px;
`

export const ServiceButton = styled(button)`
  width: 100%;
  margin: 20px auto 0;
  height: 44px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  font-weight: 700;
  font-size: 18px;
  background-color: ${({disable}) => disable ? '#a2873c' : '#FBC01E'};

  &:hover {
    cursor: ${({disable}) => disable ? 'auto !important' : 'pointer'};
    background-color: ${({disable}) => disable ? '#a2873c' : '#cda841'};
    box-shadow: ${({disable}) => disable ? 'none' : '0 0 10px 2px #323232'};
  }

  & > span {
    margin-bottom: 20px;
  }

  &:hover {
    text-decoration: none;
    cursor: ${({disable}) => disable ? 'auto !important' : 'pointer'};
    background-color: ${({disable}) => disable ? '#a2873c' : '#cda841'};
    box-shadow: ${({disable}) => disable ? 'none' : '0 0 10px 2px #323232'};
  }
`

export const Content = styled.div`
  width: 100%;
  max-width: 900px;

  @media (max-width: 900px) {
    margin-top: 30px;
    overflow-y: auto;
  }

  & * {
    font-size: 16px !important;
  }

  & svg {
    font-size: 25px !important;
  }
`

export const ServiceButton33 = styled(ServiceButton)`
  max-width: 33%;
  margin: 0;
  

  @media (max-width: 480px) {
    max-width: 100%;
    margin: 20px auto 0;
  }

  @media (min-width: 900px) {
    max-width: 100%;
    margin: 20px auto 0;
  }
`

export const Description = styled.h2`
  font-weight: 700;
  font-size: 24px !important;
  margin: 0 0 20px;

  @media (max-width: 900px) {
    margin-top: 30px;
  }
`

export const Text = styled.p`
  margin: 0 0 20px;
  text-indent: 20px;
`

export const MyTable = styled(Table)`
  border: 1px solid rgba(224, 224, 224, 1);
  width: 100%;
  max-width: 100%;

  & td {
    width: 100%;
  }
`

export const MyTableHead = styled(TableHead)`
  & * {
    color: #fff;
    width: auto;
    background-color: #313543;
  }
`