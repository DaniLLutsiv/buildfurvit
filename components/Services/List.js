import {Image, Service, ServiceButton, ServiceName, ServicePrice} from "../Home/styles";
import ImageNext from 'next/image'
import Link from 'next/link'

const List = ({list, show}) => {
    return (
        list.map(({img, name, price, id}) => (
            <Service key={id} show={show}>
                <Image>
                    <ImageNext src={img} alt={name} width={249} height={200}/>
                </Image>
                <div>
                    <ServiceName>
                        {name}
                    </ServiceName>
                    <ServicePrice>
                        {price}
                    </ServicePrice>
                    <Link href={`/service/${id}`}>
                        <a>
                            <ServiceButton>
                                <p>Заказать</p>
                            </ServiceButton>
                        </a>
                    </Link>
                </div>
            </Service>
        ))
    )
}

export default List