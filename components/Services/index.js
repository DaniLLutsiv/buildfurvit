import List from "./List";
import {ServicesList} from "./styles";

const Services = ({servicesList}) => {
    return (
        <ServicesList>
            <List show={true} list={servicesList}/>
        </ServicesList>
    )
}

export default Services