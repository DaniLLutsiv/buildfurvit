import styled from 'styled-components'

export const ServicesList = styled.div`
  display: grid;
  padding: 10px 50px;
  width: 100%;
  grid-template-columns: repeat(auto-fill, minmax(270px, 1fr));
  justify-items: center;

  & > div {
    margin-right: 20px;
    margin-bottom: 50px;
  }

  @media (max-width: 480px) {
    & > div {
      margin-right: 0;
    }
    padding: 10px 0px;
  }

  @media (max-width: 768px){
    margin-top: 35px;
  }
`