import Dialog from '@material-ui/core/Dialog';
import {CircularProgress, FormControl, Input, InputLabel, makeStyles, Slide, Snackbar} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import axios from "axios";
import {config} from "../../config";
import {
    ButtonClose,
    FormButtonModal,
    FormContainer,
    FormHeader,
    FormText,
    InfoPolicy,
    ListService,
    ListServiceName,
    ListServicesWrapper
} from "./styles";
import {InputWrapper} from "../Home/styles";
import {Flex} from "../Header/styles";
import Link from "next/link";

export default function Modal({open, setOpen, list = []}) {
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [disable, setDisable] = useState(true)
    const [loader, setLoader] = useState(false)
    const [snackbar, setSnackbar] = useState(null)
    const classes = useStyles({status: snackbar?.status});

    const handleClose = () => {
        if (!loader) {
            setOpen(false)
            setName('')
            setPhone('')
        }
    };

    useEffect(() => {
        if (name.length > 1 && /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(phone)) {
            setDisable(false)
        } else {
            setDisable(true)
        }
    }, [name, phone])

    const submit = async () => {
        if (!disable) {
            setLoader(true)
            try {
                if (typeof open === 'object') {
                    await axios.post(config.CLIENT_URL + 'form', {phone, name, data: open, list})
                } else {
                    await axios.post(config.CLIENT_URL + 'form', {phone, name})
                }

                setSnackbar({msg: 'Заказ успешно создан', status: 'success'})
                setTimeout(handleClose, 4000)
            } catch (e) {
                const msg = e.message === 'Network Error' ? 'Проверьте подключение к сети' : 'Не удалось создать заказ, попробуйте позже'
                setSnackbar({msg, status: 'error'})
            } finally {
                setLoader(false)
                setTimeout(() => setSnackbar(null), 3000)
            }
        }
    }

    return (
        <div>
            <Dialog
                open={!!open}
                onClose={handleClose}
                aria-labelledby="draggable-dialog-title"
            >

                <FormContainer>
                    <ButtonClose onClick={handleClose}>
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                            <path
                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"
                                fill="#ffffff"/>
                        </svg>
                    </ButtonClose>

                    <FormHeader>
                        Сделайте заказ прямо сейчас: <br/>{open?.name}
                    </FormHeader>

                    <FormText>
                        Точная стоимость работ определяется специалистом после осмотра. <br/>Выезд мастера — 100 грн.
                    </FormText>

                    {!!list.length && <ListServices list={list}/>}
                    <InputWrapper>
                        <FormControl>
                            <InputLabel htmlFor="standard-adornment-password">Фио</InputLabel>
                            <Input
                                id="standard-adornment-password"
                                type="text"
                                placeholder="Иванов Иван Иванович"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                            />
                        </FormControl>
                    </InputWrapper>
                    <InputWrapper>
                        <FormControl>
                            <InputLabel htmlFor="phone">Номер телефона</InputLabel>
                            <Input
                                id="phone"
                                type="text"
                                placeholder="380123456789"
                                value={phone}
                                onChange={(e) => setPhone(e.target.value)}
                            />
                        </FormControl>
                    </InputWrapper>
                    <Flex>
                        <InfoPolicy>
                            <Link href="/about?block=important">
                                <a target="_blank">
                                    Дополнительная информация
                                </a>
                            </Link>
                        </InfoPolicy>
                        <FormButtonModal disable={disable} onClick={submit}>
                            <p>Отправить</p>
                            {loader && (
                                <div className={classes.root}>
                                    <CircularProgress/>
                                </div>
                            )}
                        </FormButtonModal>
                    </Flex>
                </FormContainer>
            </Dialog>
            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                open={!!snackbar}
                onClose={handleClose}
                message={snackbar?.msg}
                key={'bottom' + 'right'}
                classes={{root: classes.snackbar}}
                TransitionComponent={TransitionUp}
            />
        </div>
    );
}


export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        position: 'absolute',
        right: '-35px',
        "& > span": {
            marginLeft: theme.spacing(1),
            width: '20px !important',
            height: '20px !important',
        },
        "& > span > svg": {
            width: '20px',
            height: '20px',
            color: '#fff',
        }
    },
    snackbar: {
        "& > div": {
            backgroundColor: ({status}) => status === 'error' ? '#f44336' : status === 'success' ? '#4caf50' : '#fff',
        }
    },
    loader: {
        display: 'flex',
        "& > span": {
            marginLeft: theme.spacing(1),
            width: '20px !important',
            height: '20px !important',
        },
        "& > span > svg": {
            width: '20px',
            height: '20px',
        }
    }
}));

export function TransitionUp(props) {
    return <Slide {...props} direction="up"/>;
}


const ListServices = ({list}) => {
    return (
        <ListServicesWrapper>
            <ListServiceName>Выбранные услуги:</ListServiceName>
            {list.map((row) => (
                <ListService key={row.id}>
                    <div>&#8226; {row.name}</div>
                    <div>{row.price} грн.</div>
                </ListService>
            ))}
        </ListServicesWrapper>
    )
}