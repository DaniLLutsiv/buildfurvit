import styled from "styled-components";
import {FormButton} from "../Home/styles";
import {button} from "../../styles";

export const FormContainer = styled.div`
  padding: 60px 60px 70px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: #373B4B;
  
  @media(max-width: 500px){
    width: 100%;
    padding: 40px 40px 50px;
  }
`

export const FormHeader = styled.div`
  color: #fff;
  font-size: 24px;

  @media (max-width: 768px) {
    font-size: 18px;
  }
`

export const InfoPolicy = styled.div`
  color: #ffffff;
  margin-top: 22px;
  cursor: pointer;
`

export const FormButtonModal = styled(FormButton)`
  margin-right: 0;
  margin-left: auto;
  margin-top: 30px;
  max-width: 150px;
  background-color: ${({disable}) => disable ? '#a2873c' : '#FBC01E'};

  &:hover {
    background-color: ${({disable}) => disable ? '#a2873c' : '#f5bc1d'};
  }

  @media(max-width: 768px) and (min-width: 500px){
    margin-right: 0;
    margin-left: auto;
  }
`

export const FormText = styled.div`
  color: #fff;
  font-size: 18px;
  margin-top: 15px;

  @media (max-width: 768px) {
    font-size: 16px;
  }
`

export const ButtonClose = styled(button)`
  position: absolute;
  top: 15px;
  right: 15px;
  background-color: inherit;
  font-size: 25px;
`

export const ListServicesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  color: #f1f1f1; 
  padding-left: 10px;
  padding-top: 10px;
`

export const ListServiceName = styled.div`
  font-size: 1.1rem;
`

export const ListService = styled.div`
  display: flex;
  justify-content: space-between;
  margin-left: 5px;
  
  & > div:first-child {
    margin-right: 5px;
  }

  & > div:last-child {
    white-space: nowrap;
  }

  @media(max-width: 500px){
    font-size: 12px;
  }
`