import {
    Input,
    NewServiceForm,
    NewServiceInput,
    SaveNewService,
    ServiceActions,
    ServiceImage,
    ServiceWrapper
} from "./styles";
import ImageIcon from "@material-ui/icons/Image";
import CloseIcon from '@material-ui/icons/Close';
import {Button, CircularProgress, TextField} from "@material-ui/core";
import {useState} from "react";
import axios from "axios";
import {config} from "../../config";
import {useStyles} from "../Modal";
import SubServices from "./SubServices";

export const NewService = ({setSnackbar, setServices}) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')
    const [img, setImg] = useState(null)
    const [loader, setLoader] = useState(null)
    const [inputList, setInputList] = useState([]);

    const mode = !(!!img && !!price && !!img && !!description)

    const save = async () => {
        setLoader(true)

        try {
            const res = await axios.post(config.CLIENT_URL + 'services', {
                name,
                img,
                price,
                list: inputList,
                description,
            })

            setServices(res.data)
            setSnackbar({status: 'success', msg: 'Сервис успешно создан'})
            clear()
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не создать сервис'})
        } finally {
            setLoader(false)
        }
    }

    const setImageHandler = (file, e) => {
        if (!file) return;

        const fr = new FileReader();
        fr.onload = function () {
            setImg(fr.result)
        }
        fr.readAsDataURL(file);

        e.target.value = ''
    }

    const clear = () => {
        setName('')
        setPrice('')
        setImg('')
        setDescription('')
        setInputList([])
    }

    return (
        <ServiceWrapper elevation={3}>
            <NewServiceForm>
                <NewServiceInput>
                    <TextField
                        autoFocus
                        id="Имя услуги"
                        value={name}
                        label="Имя услуги"
                        type="text"
                        onChange={(e) => setName(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        id="Цена услуги"
                        value={price}
                        placeholder="от 200 грн."
                        label="Цена услуги"
                        type="text"
                        onChange={(e) => setPrice(e.target.value)}
                        fullWidth
                    />
                </NewServiceInput>
                <NewServiceInput>
                    <TextField
                        autoFocus
                        id="Описание услуги"
                        value={description}
                        label="Описание услуги"
                        type="text"
                        onChange={(e) => setDescription(e.target.value)}
                        fullWidth
                        multiline
                    />

                </NewServiceInput>

                <ServiceActions>
                    <Input>
                        <label htmlFor="newFile"><ImageIcon fontSize="large"/></label>
                        <input type="file" id="newFile" accept=".jpg, .jpeg, .png"
                               onChange={(e) => setImageHandler(e.target.files[0], e)}/>
                    </Input>
                    <CloseIcon onClick={clear}/>
                </ServiceActions>
            </NewServiceForm>


            <ServiceImage>
                {!!img && (
                    <img src={img} alt={name}/>
                )}
            </ServiceImage>

            <SubServices inputList={inputList} setInputList={setInputList}/>

            <SaveNewService>
                <div>
                    <Button variant="contained" disabled={mode} onClick={save}>
                        Сохранить
                    </Button>
                    {loader && (
                        <div>
                            <CircularProgress/>
                        </div>
                    )}
                </div>
            </SaveNewService>
        </ServiceWrapper>
    )
}

export default NewService