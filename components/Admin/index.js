import {AdminWrapper} from "./styles";
import Services from "./Services";
import {useState} from "react";

const Admin = ({services}) => {
    const [serviceList, setServices] = useState(services)

    return (
        <AdminWrapper>
            <Services services={serviceList} setServices={setServices}/>
        </AdminWrapper>
    )
}

export default Admin
