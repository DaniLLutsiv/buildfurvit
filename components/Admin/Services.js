import Service from "./Service";
import {ServicesList} from "./styles";
import {TransitionUp, useStyles} from "../Modal";
import {Snackbar} from "@material-ui/core";
import {useState} from "react";
import NewService from "./NewService";

const Services = ({services, setServices}) => {
    const [snackbar, setSnackbar] = useState(null)
    const classes = useStyles({status: snackbar?.status});

    return (
        <ServicesList>
            <NewService setSnackbar={setSnackbar} setServices={setServices}/>

            {services.map(({id, name, img, price, list, description}) => (
                <Service id={id} name={name} img={img} key={id} setServices={setServices} price={price} setSnackbar={setSnackbar} list={list} description={description}/>
            ))}

            <Snackbar
                anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                open={!!snackbar}
                message={snackbar?.msg}
                key={'bottom' + 'right'}
                classes={{root: classes.snackbar}}
                TransitionComponent={TransitionUp}
            />
        </ServicesList>
    )
}

export default Services