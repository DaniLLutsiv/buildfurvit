import styled from "styled-components";
import {Paper} from "@material-ui/core";
import {Flex} from "../Header/styles";
import {button} from "../../styles";

export const AdminWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 50px;
`

export const ServicesList = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`

export const ServiceWrapper = styled(Paper)`
  width: 100%;
  position: relative;
  min-height: 300px;
  margin-right: 15px;
  margin-bottom: 30px;
  padding: 15px;
`

export const ServiceFlex = styled(Flex)`
  align-items: flex-start;
  
  @media(max-width: 768px){
    flex-direction: column-reverse;
    align-items: flex-end;
    justify-content: space-between;
  }
`

export const ServiceActions = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  & > * {
    cursor: pointer;
    margin-left: 15px;
  }
`

export const ServiceName = styled.div`
  max-width: 200px;
  font-weight: 600;
  font-size: 20px;
  word-wrap: break-word;

  @media(max-width: 768px){
    width: 100%;
    max-width: 100%;
  }
`

export const ServiceImage = styled.div`
  max-width: 100%;
  overflow: hidden;
  margin-top: 20px;
  
  & img {
    width: 100%;
  }
`

export const Input = styled.div`
  & > * {
    cursor: pointer;
  }

  & input {
    width: 0;
    height: 0;
  }
`

export const NewServiceInput = styled(Flex)`
  width: 100%;
  
  & > div {
    margin-right: 20px;
    width: 100%;
    flex-grow: 1;
  }

  @media(max-width: 768px){
    flex-direction: column;
    
    & > div {
      margin-bottom: 20px;
      margin-right: 0;
    }
  }
`

export const SaveNewService = styled.div`
  position: absolute;
  right: 30px;
  bottom: 25px;

  & > div {
    display: flex;
    position: relative;
  }
  
  & > div > div > span {
    width: 20px !important;
    height: 20px !important;
    margin-top: 6px;
    margin-left: 10px;
    color: indianred;
  }
`

export const NewServiceForm = styled(Flex)`
  margin-bottom: 10px;

  @media(max-width: 768px){
    flex-direction: column-reverse;
    align-items: flex-end;
  }
`

export const SubServicesContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
`

export const NewSubService = styled(button)`
  border: 2px solid #000;
  position: absolute;
  bottom: 25px;
  left: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  width: 30px;
  border-radius: 2px;

  &:hover {
    background-color: #e9e9e9;
  }
`

export const SubServicesList = styled.div`
  margin: 20px 0;
`

export const SubService = styled.div`

`