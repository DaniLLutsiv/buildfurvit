import {Input, NewServiceInput, ServiceActions, ServiceFlex, ServiceImage, ServiceName, ServiceWrapper} from "./styles";
import ImageIcon from "@material-ui/icons/Image";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from '@material-ui/icons/Edit';
import {useState} from "react";
import {
    Button,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField
} from "@material-ui/core";
import axios from "axios";
import {config} from "../../config";
import {useStyles} from "../Modal";
import SubServices from "./SubServices";

const Service = ({id, name, img, setServices, price, setSnackbar, list, description: desc}) => {
    const [active, setActive] = useState(null)
    const [loader, setLoader] = useState(null)
    const [value, setValue] = useState(name || '')
    const [newPrice, setNewPrice] = useState(price || '')
    const [inputList, setInputList] = useState(list || []);
    const [description, setDescription] = useState(desc || '')
    const classes = useStyles();

    const handleClose = () => {
        setActive(false);
        setLoader(false)
        setTimeout(() => setSnackbar(null), 3000)
    };

    const handleSubmit = async (value, newPrice, inputList, description) => {
        setLoader(true)

        try {
            const res = await axios.put(config.CLIENT_URL + 'services', {
                id,
                name: value,
                img,
                price: newPrice,
                list: inputList,
                description,
            })

            setServices(res.data)
            setSnackbar({status: 'success', msg: 'Имя успешно изменено'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не удалось изменить имя'})
        } finally {
            handleClose()
        }
    };

    const handleChangeImg = async (e) => {
        if (!e.target.files[0]) {
            setSnackbar({status: 'error', msg: 'файл не выбран'})
            return;
        }
        setLoader(true)

        var FR = new FileReader();

        FR.addEventListener("load", async function(e) {
            try {
                const res = await axios.put(config.CLIENT_URL + 'services', {
                    id,
                    name,
                    img,
                    price: newPrice,
                    newImg: e.target.result,
                    list: inputList,
                })

                setServices(res.data)
                setSnackbar({status: 'success', msg: 'Картинка успешно изменена'})
            } catch (e) {
                setSnackbar({status: 'error', msg: 'Не удалось изменить картинку'})
            } finally {
                handleClose()
            }
        });

        FR.readAsDataURL(e.target.files[0]);
    };

    const handleDelete = async () => {
        setLoader(true)

        try {
            const res = await axios.delete(config.CLIENT_URL + 'services',
                {
                    data: {
                        id
                    }
                })

            setServices(res.data)
            setSnackbar({status: 'success', msg: 'Сервис успешно удалён'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не удалить сервис'})
        } finally {
            handleClose()
        }
    };

    return (
        <ServiceWrapper elevation={3}>
            <ServiceFlex>
                <ServiceName>
                    Услуга: {name}
                </ServiceName>
                <ServiceName>
                    Стоимость: {price}
                </ServiceName>

                <ServiceActions>
                    <EditIcon fontSize="large" onClick={() => setActive({
                        header: "Введите новое имя",
                        text: "После сохранения имя поменяеться на всём сайте",
                        button: "Сохранить",
                        viewInput: true,
                        handler: handleSubmit
                    })}/>

                    <Input>
                        <label htmlFor={`file${id}`}><ImageIcon fontSize="large"/></label>
                        <input type="file" id={`file${id}`} accept=".jpg, .jpeg, .png" onChange={(e) => handleChangeImg(e)}/>
                    </Input>

                    <DeleteIcon fontSize="large" onClick={() => setActive({
                        header: `Вы точно хотите удалить услугу: ${name}`,
                        text: "После сохранения это действие нельзя будет отменить",
                        button: "Удалить",
                        handler: handleDelete
                    })}/>
                </ServiceActions>
            </ServiceFlex>


            <ServiceImage>
                <img src={img} alt={name}/>
            </ServiceImage>

            <Dialog open={!!active} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{active?.header}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {active?.text}
                    </DialogContentText>
                    {active?.viewInput && (
                        <>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                value={value}
                                label="Имя услуги"
                                type="text"
                                onChange={(e) => setValue(e.target.value)}
                                fullWidth
                            />

                            <TextField
                                margin="dense"
                                id="price"
                                value={newPrice}
                                label="Цена услуги"
                                placeholder="от 200 грн."
                                type="text"
                                onChange={(e) => setNewPrice(e.target.value)}
                                fullWidth
                            />
                            <TextField
                                id="Описание услуги"
                                margin="dense"
                                value={description}
                                label="Описание услуги"
                                type="text"
                                onChange={(e) => setDescription(e.target.value)}
                                fullWidth
                                multiline
                            />

                            <SubServices inputList={inputList} setInputList={setInputList}/>
                        </>
                    )}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Отмена
                    </Button>
                    <Button onClick={() => active?.handler(value, newPrice, inputList, description)}
                            color="primary"
                            disabled={active?.button === 'Удалить' ? false : !(!!value && !!newPrice)}>
                        <p>{active?.button}</p>
                        {loader && (
                            <div className={classes.loader}>
                                <CircularProgress color="primary"/>
                            </div>
                        )}
                    </Button>
                </DialogActions>
            </Dialog>
        </ServiceWrapper>
    )
}

export default Service