import {useState} from "react";
import {
    NewServiceForm,
    NewServiceInput,
    NewSubService,
    ServiceActions,
    SubServicesContainer,
    SubServicesList
} from "./styles";
import AddIcon from '@material-ui/icons/Add';
import {TextField} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const SubServices = ({inputList, setInputList}) => {

    // handle input change
    const handleInputChange = (e, index) => {
        const {name, value} = e.target;
        const list = [...inputList];
        list[index][name] = value;
        setInputList(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...inputList];
        list.splice(index, 1);
        setInputList(list);
    };

    const handleAddClick = () => {
        const prev = inputList[inputList.length - 1]?.id
        const id = (prev !== undefined && prev + 1) || 0
        setInputList([...inputList, { name: "", price: "", id, units: "" }]);
    };

    return (
        <SubServicesContainer>
            <SubServicesList>
                {inputList.map((x, i) => (
                    <NewServiceForm key={x.id}>
                        <NewServiceInput>
                            <TextField
                                id={`Имя услуги ${x.id}`}
                                value={x.name}
                                label="Имя услуги"
                                type="text"
                                name="name"
                                onChange={e => handleInputChange(e, i)}
                                fullWidth
                            />
                            <TextField
                                id={`Цена услуги ${x.id}`}
                                value={x.price}
                                label="Цена услуги"
                                placeholder="150"
                                type="text"
                                name="price"
                                onChange={e => handleInputChange(e, i)}
                                fullWidth
                            />
                            <TextField
                                id={`Единицы измерения ${x.id}`}
                                value={x.units}
                                label="Единицы измерения"
                                type="text"
                                name="units"
                                onChange={e => handleInputChange(e, i)}
                                fullWidth
                            />
                        </NewServiceInput>

                        <ServiceActions>
                            <CloseIcon onClick={() => handleRemoveClick(i)}/>
                        </ServiceActions>
                    </NewServiceForm>
                ))}
            </SubServicesList>

            <NewSubService onClick={handleAddClick}>
                <AddIcon/>
            </NewSubService>
        </SubServicesContainer>
    )
}

export default SubServices