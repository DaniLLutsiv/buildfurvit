import {
    CloseIcon,
    Flex,
    HeaderTop,
    HeaderWrapper,
    Menu,
    MenuIconWrapper,
    MenuItem,
    MobileMenu,
    MobileMenuItem,
    MobileMenuWrapper
} from "./styles";
import PhoneIcon from '@material-ui/icons/Phone';
import MenuIcon from '@material-ui/icons/Menu';
import {useEffect, useRef, useState} from "react";
import Link from 'next/link'
import {useRouter} from "next/router";
import {config} from "../../config";

const Header = ({admin}) => {
    const ref = useRef()
    const [open, setOpen] = useState(null)
    const router = useRouter();

    useEffect(() => setOpen(false), [])

    return (
        <>
            <HeaderWrapper>
                <Flex className="syllable">БЫСТРОЕ РЕШЕНИЕ БЫТОВЫХ ЗАДАЧ</Flex>
                <Flex>
                    <PhoneIcon style={{color: '#FBC01E'}} className="phoneIcon"/>
                    <a href={`tel:${config.PHONE_1}`}>{config.PHONE_1_STYLE}</a>
                    <a href={`tel:${config.PHONE_2}`}>{config.PHONE_2_STYLE}</a>
                    <MenuIconWrapper>
                        <MenuIcon ref={ref} onClick={() => setOpen(true)}/>
                    </MenuIconWrapper>
                </Flex>
            </HeaderWrapper>

            <HeaderTop>
                <MobileMenuWrapper open={open}>
                    {open && (
                        <MobileMenu>
                            <CloseIcon onClick={() => setOpen(false)}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                                    <path
                                        d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"/>
                                </svg>
                            </CloseIcon>

                            <Link href="/"><a>
                                <MobileMenuItem active={router.pathname === '/'}>
                                    Главная
                                </MobileMenuItem>
                            </a></Link>
                            <Link href="/about"><a>
                                <MobileMenuItem active={router.pathname === '/about'}>
                                    О нас
                                </MobileMenuItem>
                            </a></Link>

                            <Link href="/services"><a>
                                <MobileMenuItem active={router.pathname === '/services' || router.pathname.startsWith('/service')}>
                                    Услуги
                                </MobileMenuItem>
                            </a></Link>
                            <Link href="/contact"><a>
                                <MobileMenuItem active={router.pathname === '/contact'}>
                                    Контакты
                                </MobileMenuItem>
                            </a></Link>
                            <Link href="/reviews"><a>
                                <MobileMenuItem active={router.pathname === '/reviews'}>
                                    Отзывы
                                </MobileMenuItem>
                            </a></Link>

                            {admin && (
                                <Link href="/admin"><a>
                                    <MobileMenuItem active={router.pathname === '/admin'}>
                                        Админ
                                    </MobileMenuItem>
                                </a></Link>
                            )}
                        </MobileMenu>
                    )}
                </MobileMenuWrapper>

            </HeaderTop>


            {open !== null && (
                <Menu>
                    <Link href="/"><a>
                        <MenuItem active={router.pathname === '/'}>Главная</MenuItem>
                    </a></Link>
                    <Link href="/about"><a>
                        <MenuItem active={router.pathname === '/about'}>О нас</MenuItem>
                    </a></Link>
                    <Link href="/services"><a>
                        <MenuItem active={router.pathname === '/services' || router.pathname.startsWith('/service')}>Услуги</MenuItem>
                    </a></Link>
                    <Link href="/contact"><a>
                        <MenuItem active={router.pathname === '/contact'}>Контакты</MenuItem>
                    </a></Link>
                    <Link href="/reviews"><a>
                        <MenuItem active={router.pathname === '/reviews'}>Отзывы</MenuItem>
                    </a></Link>

                    {admin && (
                        <Link href="/admin"><a>
                            <MenuItem active={router.pathname === '/admin'}>Админ</MenuItem>
                        </a></Link>
                    )}
                </Menu>
            )}
        </>
    )
}

export default Header