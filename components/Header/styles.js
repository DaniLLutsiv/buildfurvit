import styled from "styled-components"

export const Flex = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const HeaderWrapper = styled.header`
  height: 50px;
  color: #9FA2AE;
  width: 100%;
  background-color: #2C2C2C;
  display: flex;
  justify-content: space-between;
  align-items: center;


  & > div {
    max-width: 1500px;
    padding: 0 20px;
  }

  & > div > * {
    margin-right: 10px;
  }

  & > div > div {
    margin-left: 5px;
  }

  & svg {
    cursor: pointer;
  }

  @media (max-width: 768px) {
    position: fixed;
    top: 0;
    z-index: 5;
    left: 0;
    width: 100%;
    background-color: #2C2C2C;

    .syllable {
      display: none;
    }

    & > div {
      justify-content: flex-end;

      width: 100%;

      & > svg {
        margin-right: 5px;
      }
    }

    & > div > div {
      font-size: 14px;
      margin-right: 0;
      white-space: nowrap;
    }
  }

  @media (max-width: 400px) {
    font-size: 14px;
  }

  @media (max-width: 370px) {
    font-size: 12px;
  }
`

export const HeaderTop = styled.div`
  color: #9FA2AE;
  display: flex;
  justify-content: space-between;
  padding: 0 20px;
  max-width: 1500px;
  margin: 0 auto;
  position: relative;

  @media (max-width: 768px) {
    position: fixed;
    top: 0;
    z-index: 5;
    left: 0;
    width: 100%;
    background-color: #2C2C2C;
`

export const Menu = styled.div`
  display: flex;
  height: 50px;
  width: 100%;
  left: 0;
  background-color: #f1f1f2;
  padding: 0 20px;
  position: sticky;
  top: -1px;
  box-shadow: 0 1px 1px rgb(0 0 0 / 30%);
  z-index: 5;

  @media (max-width: 768px) {
    display: none;
  }
`

export const MenuItem = styled.div`
  padding: 10px 25px;
  color: #1f1f1f;
  background-color: ${({active}) => active ? '#fbc32b' : 'inherit'};
  font-weight: 600;
  font-size: 18px;
  height: 100%;
  border-right: 1px solid #e0e0e0;

  &:first-child {
    border-left: 1px solid #e0e0e0;
  }

  &:hover {
    background-color: ${({active}) => active ? '#ecb71f' : '#e3e3e3'};
  }
`

export const MobileMenuWrapper = styled(Flex)`
  position: fixed;
  width: ${({open}) => open ? '100vw !important' : '0 !important'};
  left: 0;
  top: 0;
  height: 100vh;
  align-items: flex-start;
  transition: width .5s;
  background-color: #2C2C2C;
  z-index: 10;

  @media (min-width: 769px) {
    display: none;
  }
`

export const MobileMenu = styled(Flex)`
  position: relative;
  padding-top: 60px;
  width: 100%;
  margin-left: 0 !important;
  flex-direction: column;

  & > a {
    width: 100%;
  }
`

export const MobileMenuItem = styled(Flex)`
  border-top: 1px solid #1f1f1f;
  height: 60px;
  width: 100%;
  color: ${({active}) => active ? '#fbc01e' : '#f3f3f3'};;
  align-items: center;
  padding-left: 20px;
  font-size: 20px;

  &:hover {
    background-color: #1f1f1f;
  }
`

export const CloseIcon = styled.div`
  position: absolute;
  right: 30px;
  top: 15px;
  width: 31px;
  height: 31px;

  & > svg {
    margin-left: 3px;
    margin-top: 3px;
    fill: #f5f5f5;
    width: 25px;
    height: 25px;
  }

  &:hover {
    background-color: #1f1f1f;
    border-radius: 50%;
  }
`

export const MenuIconWrapper = styled.div`
  display: flex;
  align-items: center;
  color: #fff;

  @media (min-width: 769px) {
    display: none;
  }
`