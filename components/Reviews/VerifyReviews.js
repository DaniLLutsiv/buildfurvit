import {Actions, ActionsVerify, Review, ReviewAnswer, ReviewBody, ReviewHeader, ReviewHeaderDate} from "./styles";
import {CircularProgress, TextField} from "@material-ui/core";
import {FormButtonModal} from "../Modal/styles";
import {useContext, useState} from "react";
import {useStyles} from "../Modal";
import axios from "axios";
import {config} from "../../config";
import {SnackbarContext} from "../../pages/_app";
import DeleteIcon from "@material-ui/icons/Delete";
import {Flex} from "../Header/styles";
import {button as Button} from "../../styles/index";

const VerifyReviews = ({list, setList, setActive}) => {
    const {setSnackbar} = useContext(SnackbarContext)

    return (
        <>
            {list.map(({name, id, description, date, email}) => (
                <VerifyReview setList={setList} setSnackbar={setSnackbar} name={name} id={id} key={id}
                              description={description} date={date} email={email} setActive={setActive}/>
            ))}
        </>
    )
}

export default VerifyReviews


const VerifyReview = ({name, id, date, description, email, setSnackbar, setList, setActive}) => {
    const [answer, setAnswer] = useState('')
    const [loader, setLoader] = useState(false)
    const classes = useStyles();

    const handlerClickDelete = (id, name) => {
        setActive({
            data: {
                id
            },
            header: `Вы точно хотите удалить комментарий: ${name}`,
            text: "После сохранения это действие нельзя будет отменить",
            button: "Удалить",
            handler: deleteHandle
        })
    }

    const deleteHandle = async (id) => {
        setLoader(true)

        try {
            const res = await axios.delete(config.CLIENT_URL + 'reviews',
                {
                    data: {
                        id
                    }
                })

            setList(res.data)
            setSnackbar({status: 'success', msg: 'Комментарий успешно удалён'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не удалось удалить комментарий'})
        } finally {
            setActive(null)
        }
    }


    const submit = async (name, id, description, date, answer) => {
        setLoader(true)

        try {
            const data = await axios.put(config.CLIENT_URL + 'reviews', {name, id, email, description, date, answer})

            setSnackbar({msg: 'Отзыв успешно сохранён', status: 'success'})
            setList(data.data)
        } catch (e) {
            const msg = e.message === 'Network Error' ? 'Проверьте подключение к сети' : 'Отзыв не сохранён попробуйте позже'
            setSnackbar({msg, status: 'error'})
        } finally {
            setLoader(false)
        }
    }

    return (
        <Review>
            <ReviewHeader>
                <div>{name}</div>
                <Flex>
                    <ReviewHeaderDate>
                        {date}
                    </ReviewHeaderDate>
                    <Button>
                        <DeleteIcon fontSize="large" onClick={() => handlerClickDelete(id, name)}/>
                    </Button>
                </Flex>
            </ReviewHeader>
            <ReviewBody>
                <div>{description}</div>
            </ReviewBody>

            <ActionsVerify>
                <TextField
                    autoFocus
                    id="Ответ2"
                    value={answer}
                    label="Ответ"
                    type="text"
                    onChange={(e) => setAnswer(e.target.value)}
                    fullWidth
                    className="input"
                />

                <FormButtonModal onClick={() => submit(name, id, description, date, answer)}>
                    <p>Сохранить</p>
                    {loader && (
                        <div className={classes.root}>
                            <CircularProgress/>
                        </div>
                    )}
                </FormButtonModal>
            </ActionsVerify>
        </Review>
    )
}