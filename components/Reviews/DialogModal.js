import {
    Button,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField
} from "@material-ui/core";
import {useState} from "react";
import {useStyles} from "../Modal";
import {LoaderWrapper} from "./styles";

const DialogModal = ({active, setActive, loader}) => {
    const [name, setName] = useState(active?.data?.name || '')
    const [date, setDate] = useState(active?.data?.date || '')
    const [text, setText] = useState(active?.data?.text || '')
    const [answer, setAnswer] = useState(active?.data?.answer || '')
    const classes = useStyles();

    const handleClose = () => {
        setActive(null)
    }


    return (
        <Dialog open={!!active} onClose={handleClose} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">{active?.header}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {active?.text}
                </DialogContentText>
                {active?.viewInput && (
                    <>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={name}
                            label="Имя автора"
                            type="text"
                            onChange={(e) => setName(e.target.value)}
                            fullWidth
                        />

                        <TextField
                            autoFocus
                            margin="dense"
                            id="price"
                            value={text}
                            label="Комментарий"
                            type="text"
                            onChange={(e) => setText(e.target.value)}
                            fullWidth
                        />

                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={date}
                            label="Дата"
                            type="text"
                            onChange={(e) => setDate(e.target.value)}
                            fullWidth
                        />

                        <TextField
                            autoFocus
                            margin="dense"
                            id="price"
                            value={answer}
                            label="Ответ"
                            type="text"
                            onChange={(e) => setAnswer(e.target.value)}
                            fullWidth
                        />
                    </>
                )}
            </DialogContent>

            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Отмена
                </Button>
                <Button onClick={() => active?.handler(active?.data?.id, name, text, date, answer)}
                        color="primary"
                        disabled={active?.button === 'Удалить' ? false : !(!!name && !!text && !!date)}>
                    <p>{active?.button}</p>
                    <LoaderWrapper>
                        {loader && (
                            <div className={classes.root}>
                                <CircularProgress/>
                            </div>
                        )}
                    </LoaderWrapper>
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default DialogModal