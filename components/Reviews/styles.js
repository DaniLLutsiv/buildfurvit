import styled from 'styled-components'
import {InputWrapper} from "../Home/styles";

export const Reviews = styled.div`
  width: 100%;
  overflow: hidden;
  padding: 100px 0;

  & > img {
    object-fit: contain;
    max-width: 100%;
    width: auto;
    position: absolute;
    top: 0;
    z-index: -1;
  }

`

export const Review = styled.div`
  background-color: #fff;
  margin-bottom: 30px;
  width: 80%;
  margin-left: 10%;
  min-height: 100px;
  border-radius: 4px;
  box-shadow: 0 0 10px 1px #d0caca;
  word-break: break-word;
`

export const ReviewHeader = styled.div`
  display: flex;
  padding: 20px;
  position: relative;
  justify-content: space-between;
  font-weight: 700;
  align-items: center;
  font-size: 18px;

  &:after {
    content: '';
    border-bottom: 1px solid #e1e1e7;
    display: block;
    position: absolute;
    width: calc(100% - 40px);
    left: 20px;
    bottom: 5px;
  }
`

export const ReviewHeaderDate = styled.div`
  font-size: 14px;
  font-weight: 400;
  margin-left: 20px;
  min-width: 100px;
  text-align: right;
`

export const ReviewBody = styled.div`
  padding: 20px;
  color: #333;
`

export const ReviewAnswer = styled(ReviewBody)`
  padding-top: 0;

  &:before {
    content: '';
    border-bottom: 1px solid #e1e1e7;
    display: block;
    width: 100%;
    margin-bottom: 20px;
  }
`

export const NewReviewsContainer = styled.div`
  & > div {
    padding: 20px;

    & > div {
      margin-bottom: 20px;
      align-items: flex-end;
    }
  }
`

export const ButtonContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-bottom: 0 !important;

  & > div > span {
    width: 20px !important;
    height: 20px !important;
    margin-bottom: 5px;
    margin-left: 10px;
    color: indianred;
  }
`

export const Actions = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  padding-right: 15px;
  padding-bottom: 15px;

  & > * {
    cursor: pointer;
    margin-left: 15px;
  }
`

export const ActionsVerify = styled(Actions)`
  align-items: flex-end;
  
  & .input {
    height: 56px;
  }
  
  & > * {
    &:hover {
      box-shadow: ${({disable}) => disable ? 'none' : '0 0 10px 2px #ffffff !important'};
    }
  }
`

export const LoaderWrapper = styled.div`
  width: 30px;
  height: 30px;

  & svg {
    color: indianred !important;
    position: relative !important;
    top: auto !important;
    right: auto !important;
  }

  & div {
    margin-top: 4px;
    right: auto !important;
  }
`

export const FormWrapper = styled(Review)`
  background-color: inherit;
  box-shadow: none;
`

export const FormInputWrapper = styled(InputWrapper)`
  max-width: 800px;

  & .ml {
    margin-left: 20px !important;
  }

  & > div {
    display: flex;
    width: 100%;
    flex-direction: row;

    &:not(:last-of-type) {
      margin-bottom: 20px;
    }

    &:last-of-type {
      & > div {
        max-width: 100% !important;
      }
    }
  }


  & > div > div {
    max-width: 50%;
    background-color: #f6f6f6;
  }

  & * {
    border-color: #333333 !important;
  }

  & label {    
    color: #333333 !important;
  }

  & span {
    position: absolute;
    width: 20px !important;
    height: 20px !important;
    right: -30px;
    top: 10px;
    color: indianred;
  }

  & > button {
    position: relative;
  }

  @media (min-width: 769px) {
    & > button {
      margin-left: 0;

      &:hover {
        box-shadow: ${({disable}) => disable ? 'none' : '0 0 10px 2px #ffffff'};
      }
    }
  }
`

export const FormName = styled.div`
  font-weight: 700;
  font-size: 23px;
  line-height: 1.4;
  margin-bottom: 20px;
`