import {ButtonContainer, NewReviewsContainer, Review} from "./styles";
import {Button, CircularProgress, TextField} from "@material-ui/core";
import {useState} from "react";
import axios from "axios";
import {config} from "../../config";

const NewReviews = ({setList, setSnackbar}) => {
    const [name, setName] = useState('')
    const [date, setDate] = useState('')
    const [text, setText] = useState('')
    const [answer, setAnswer] = useState('')
    const [loader, setLoader] = useState(false)
    const mode = !(name && date && text)

    const save = async () => {
        setLoader(true)

        try {
            const res = await axios.post(config.CLIENT_URL + 'reviews', {
                name,
                date,
                description: text,
                answer,
            })

            setList(res.data)
            setName('')
            setDate('')
            setText('')
            setAnswer('')

            setSnackbar({status: 'success', msg: 'Комментарий успешно создан'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не создать комментарий'})
        } finally {
            setLoader(false)
        }
    }

    return (
        <NewReviewsContainer>
            <Review>
                <TextField
                    autoFocus
                    id="Имя автора"
                    value={name}
                    label="Имя автора"
                    type="text"
                    onChange={(e) => setName(e.target.value)}
                    fullWidth
                />

                <TextField
                    autoFocus
                    id="Дата"
                    value={date}
                    label="Дата"
                    type="text"
                    onChange={(e) => setDate(e.target.value)}
                    fullWidth
                />

                <TextField
                    autoFocus
                    id="Комментарий"
                    value={text}
                    label="Комментарий"
                    type="text"
                    onChange={(e) => setText(e.target.value)}
                    fullWidth
                />

                <TextField
                    autoFocus
                    id="Ответ"
                    value={answer}
                    label="Ответ"
                    type="text"
                    onChange={(e) => setAnswer(e.target.value)}
                    fullWidth
                />

                <ButtonContainer>
                    <Button variant="contained" disabled={mode} onClick={save}>
                        Сохранить
                    </Button>
                    {loader && (
                        <div>
                            <CircularProgress/>
                        </div>
                    )}
                </ButtonContainer>
            </Review>



        </NewReviewsContainer>
    )
}

export default NewReviews
