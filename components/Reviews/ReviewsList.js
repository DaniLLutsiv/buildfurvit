import dynamic from 'next/dynamic'
import {Actions, Review, ReviewAnswer, ReviewBody, ReviewHeader, ReviewHeaderDate, Reviews} from "./styles";
const NewReviews = dynamic(() => import('./NewReviews'))
const VerifyReviews = dynamic(() => import('./VerifyReviews'))
import DeleteIcon from "@material-ui/icons/Delete";
import DialogModal from "./DialogModal";
import {useContext, useState} from "react";
import EditIcon from "@material-ui/icons/Edit";
import axios from "axios";
import {config} from "../../config";
import FormReviews from "./FormReviews";
import {SnackbarContext} from "../../pages/_app";

const ReviewsList = ({list, admin, setList}) => {
    const [active, setActive] = useState(null)
    const [loader, setLoader] = useState(false)
    const {setSnackbar} = useContext(SnackbarContext)

    const handlerClose = () => {
        setLoader(false)
        setTimeout(() => setSnackbar(null), 3000)
    }

    const handlerClickEdit = (id, name, date, text, answer) => {
        setActive({
            data: {
                id,
                name,
                date,
                text,
                answer,
            },
            header: "Введите новое данные",
            text: "После сохранения они поменяеться на всём сайте",
            button: "Сохранить",
            viewInput: true,
            handler: editHandler
        })
    }

    const handlerClickDelete = (id, name) => {
        setActive({
            data: {
                id
            },
            header: `Вы точно хотите удалить комментарий: ${name}`,
            text: "После сохранения это действие нельзя будет отменить",
            button: "Удалить",
            handler: deleteHandle
        })
    }

    const deleteHandle = async (id) => {
        setLoader(true)

        try {
            const res = await axios.delete(config.CLIENT_URL + 'reviews',
                {
                    data: {
                        id
                    }
                })

            setList(res.data)
            setActive(null)
            setSnackbar({status: 'success', msg: 'Комментарий успешно удалён'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не удалось удалить комментарий'})
        } finally {
            handlerClose()
        }
    }

    const editHandler = async (id, name, text, date, answer) => {
        setLoader(true)

        try {
            const res = await axios.put(config.CLIENT_URL + 'reviews', {
                id,
                name,
                description: text,
                date,
                answer,
            })

            setList(res.data)
            setActive(null)
            setSnackbar({status: 'success', msg: 'Комментарий успешно измененён'})
        } catch (e) {
            setSnackbar({status: 'error', msg: 'Не удалось изменить комментарий'})
        } finally {
            handlerClose()
        }
    }

    return (
        <Reviews style={{backgroundImage: `url('/images/bg.jpg')`}}>
            {!!admin && (<NewReviews setList={setList} setSnackbar={setSnackbar}/>)}

            {list.filter(el => !el.real).map(({name, id, description, date, answer}) => (
                <Review key={id}>
                    <ReviewHeader>
                        <div>{name}</div>
                        <ReviewHeaderDate>{date}</ReviewHeaderDate>
                    </ReviewHeader>
                    <ReviewBody>
                        <div>{description}</div>
                    </ReviewBody>
                    {answer && (
                        <ReviewAnswer>
                            <div><b>Админ:</b> {answer}</div>
                        </ReviewAnswer>
                    )}
                    {admin && (
                        <Actions>
                            <EditIcon fontSize="large"
                                      onClick={() => handlerClickEdit(id, name, date, description, answer)}/>

                            <DeleteIcon fontSize="large" onClick={() => handlerClickDelete(id, name)}/>
                        </Actions>

                    )}
                </Review>
            ))}

            {admin && <VerifyReviews list={list.filter(el => el.real)} setList={setList} setActive={setActive}/>}

            <FormReviews setList={setList}/>

            {active && <DialogModal active={active} setActive={setActive} loader={loader}/>}
        </Reviews>
    )
}

export default ReviewsList