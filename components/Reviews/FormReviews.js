import {FormInputWrapper, FormName, FormWrapper} from "./styles";
import {CircularProgress, FormControl, TextField} from "@material-ui/core";
import {useContext, useState} from "react";
import {FormButtonModal} from "../Modal/styles";
import {useStyles} from "../Modal";
import axios from "axios";
import {config} from "../../config";
import {SnackbarContext} from '../../pages/_app'
import {formattedDate} from "../../helpers/formattedDate";

const FormReviews = ({setList}) => {
    const classes = useStyles();
    const {setSnackbar} = useContext(SnackbarContext)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [text, setText] = useState('')
    const [loader, setLoader] = useState(false)
    const disable = !(name && text.length > 10 && /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/.test(email))

    const submit = async () => {
        if (!disable){
            setLoader(true)
            try {
                const data = await axios.post(config.CLIENT_URL + 'reviews', {name, email, description: text, date: formattedDate(), real: true})
                setSnackbar({msg: 'Комментарий успешно создан, он будет добавлен на сайт после просмотра его администратором', status: 'success'})
                setList(data.data)
                setName('')
                setEmail('')
                setText('')
            } catch (e) {
                const msg = e.message === 'Network Error' ? 'Проверьте подключение к сети' : 'Не удалось создать коментарий, попробуйте позже'
                setSnackbar({msg, status: 'error'})
            } finally {
                setLoader(false)
                setTimeout(() => setSnackbar(null), 10000)
            }

        }
    }

    return (
        <FormWrapper>
            <FormInputWrapper>
                <FormName>
                    Оставить отзыв
                </FormName>

                <FormControl>
                    <TextField placeholder="Фио" id="name" variant="outlined" value={name} onChange={(e) => setName(e.target.value)}/>
                    <TextField placeholder="Почта" id="email" variant="outlined" value={email} className="ml" onChange={(e) => setEmail(e.target.value)}/>
                </FormControl>

                <FormControl>
                    <TextField
                        id="outlined-multiline-static"
                        placeholder="Комментарий, минимум 10 символов"
                        multiline
                        rows={8}
                        value={text}
                        onChange={(e) => setText(e.target.value)}
                        variant="outlined"
                    />
                </FormControl>

                <FormButtonModal disable={disable} onClick={submit}>
                    <p>Отправить</p>
                    {loader && (
                        <div className={classes.root}>
                            <CircularProgress/>
                        </div>
                    )}
                </FormButtonModal>
            </FormInputWrapper>
        </FormWrapper>
    )
}

export default FormReviews