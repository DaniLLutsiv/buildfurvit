import styled from "styled-components";
import {button} from "../../styles";

export const FooterWrapper = styled.footer`
  background-color: #2C2C2C;
  min-height: 200px;
  width: 100%;

  & > div {
    max-width: 1500px;
    margin: auto;
  }
`

export const FooterContainer = styled.div`
  display: flex;
  padding: 30px 20px;
  justify-content: space-between;
  overflow: hidden;

  @media (max-width: 768px) {
    flex-direction: column;

    & > div {
      margin-bottom: 30px;
    }
  }
`

export const FooterItem = styled.div`
  padding: 0 20px;
  display: flex;
  flex-direction: column;
  color: #e5e5e5;

  @media(max-width: 768px){
    &:after {
      content: ${({last}) => last ? 'none' : '""'};
      display: block;
      width: 100%;
      margin-top: 10px;
      border-bottom: 1px solid #7a8098;
      opacity: .2;
    }

    &:last-child:after {
      content: none;
    }
  }
`

export const ItemName = styled.div`
  font-weight: 700;
  font-size: 18px;
  margin-bottom: 5px;
`

export const Contact = styled.div`
  margin-top: 10px;
  font-size: 16px;
  font-weight: 600;
`

export const MenuItem = styled.a`
  font-weight: 500;
  margin-top: 10px;
  font-size: 16px;
  color: inherit;
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`

export const FooterButton = styled(button)`
  padding: 15px;
  margin-top: 20px;
  background-color: #fbc32b;
  border-radius: 5px;
  height: 52px;
  width: 100%;
  margin-left: auto;
  font-weight: 600;
  font-size: 20px;
  margin-bottom: 10px;

  &:hover {
    background-color: #eab628;
  }

  @media (max-width: 768px) {
    margin-left: 0;
    max-width: 200px;
    margin-bottom: 15px;
  }
`

export const Name = styled.div`
  color: #babecd;
  margin-top: 10px;
  font-size: 20px;
`

export const Text = styled(Name)`
  font-size: 16px;
  width: 260px;
`