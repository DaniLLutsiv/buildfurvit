import {
    Contact,
    FooterButton,
    FooterContainer,
    FooterItem,
    FooterWrapper,
    ItemName,
    MenuItem,
    Name,
    Text
} from "./styles";
import Link from 'next/link'
import {config} from "../../config";
import {useRouter} from "next/router";
import {useState} from "react";
import Modal from "../Modal";

const Footer = ({refForm, inputRef}) => {
    const router = useRouter();
    const [viewModal, setViewModal] = useState(false)

    const handler = () => {
        if (router.pathname === '/'){

            refForm.current.scrollIntoView()
            inputRef.current.focus()
        } else {
            setViewModal(true)
        }
    }

    return (
        <FooterWrapper>
            <FooterContainer>
                <FooterItem>
                    <ItemName>
                        BuildFurVit
                    </ItemName>
                    <Name>
                        Смиюха Виталий Степанович
                    </Name>
                </FooterItem>
                <FooterItem>
                    <ItemName>
                        МЕНЮ
                    </ItemName>
                    <Link href="/about">
                        <MenuItem>
                            О нас
                        </MenuItem>
                    </Link>
                    <Link href="/services">
                        <MenuItem>
                            Услуги
                        </MenuItem>
                    </Link>
                    <Link href="/contact">
                        <MenuItem>
                            Контакты
                        </MenuItem>
                    </Link>
                    <Link href="/reviews">
                        <MenuItem>
                            Отзывы
                        </MenuItem>
                    </Link>
                </FooterItem>
                <FooterItem>
                    <ItemName>
                        КОНТАКТЫ
                    </ItemName>
                    <Contact>
                        <a href={`tel:${config.PHONE_1}`}>{config.PHONE_1_STYLE}</a>
                    </Contact>
                    <Contact>
                        <a href={`tel:${config.PHONE_2}`}>{config.PHONE_2_STYLE}</a>
                    </Contact>
                    <Contact>
                        BuildFurVit@gmail.com
                    </Contact>
                    <Contact>
                        г. Запорожье
                    </Contact>
                </FooterItem>
                <FooterItem last>
                    <ItemName>
                        ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ
                    </ItemName>
                    <FooterButton onClick={handler}>
                        Заказать звонок
                    </FooterButton>
                    <Text>
                        Остались вопросы? Закажите звонок и мы позвоним в удобное для Вас время
                    </Text>
                </FooterItem>
                <Modal open={viewModal} setOpen={setViewModal}/>
            </FooterContainer>
        </FooterWrapper>
    )
}

export default Footer