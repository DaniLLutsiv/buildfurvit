import styled from 'styled-components'
import {Flex} from "../Header/styles";

export const AboutWrapper = styled.div`
  width: 100%;
  padding: 0 20px;
  margin-bottom: 40px;

  & section {
    line-height: 1.8;
    font-size: 18px;
    padding: 10px 20px 0px;
  }

  ul {
    margin: 10px 0;
  }

  @media (max-width: 768px) {
    margin-top: 35px;
  }

  @media (max-width: 480px) {
    padding: 0;
    
    p {
      margin-top: 0;
    }
  }
`

export const FlexSlider = styled(Flex)`
  flex-direction: initial;
  width: 100%;
  
  & > div {
    width: 50%;
    margin-right: 10px;
  }
  
  @media(max-width: 768px){
    flex-direction: column;
    
    & > div {
      width: 100%;
    }
    
    & > *:first-child{
      margin-right: 0;
      margin-bottom: 10px;
    }
  }
`