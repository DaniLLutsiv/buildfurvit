import {AboutWrapper, FlexSlider} from "./styles";
import {HeaderH1} from "../Home/styles";
import React, {useEffect, useMemo, useRef} from "react";
import Slider from "../Home/Slider";
import {useRouter} from "next/router";

export const badRooms = [
    {
        original: '/images/slider/badroom/1.jpg',
        originalAlt: "ремонт кухни",
    },
    {
        original: '/images/slider/badroom/2.jpg',
        originalAlt: "ремонт кухни",
    },
    {
        original: '/images/slider/badroom/3.jpg',
        originalAlt: "ремонт кухни",
    },
]

export const kitchen = [
    {
        original: '/images/slider/kitchen/1.jpg',
        originalAlt: "ремонт спальни",
    },
    {
        original: '/images/slider/kitchen/2.jpg',
        originalAlt: "ремонт спальни",
    },
    {
        original: '/images/slider/kitchen/3.jpg',
        originalAlt: "ремонт спальни",
    },
    {
        original: '/images/slider/kitchen/4.jpg',
        originalAlt: "ремонт спальни",
    },
    {
        original: '/images/slider/kitchen/5.jpg',
        originalAlt: "ремонт спальни",
    },
]

export const rooms = [
    {
        original: '/images/slider/livingRoom/1.jpg',
        originalAlt: "ремонт гостиной",
    },/*
    {
        original: '/images/slider/livingRoom/2.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/3.jpg',
        originalAlt: "ремонт гостиной",
    },*/
    {
        original: '/images/slider/livingRoom/4.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/5.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/6.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/7.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/8.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/9.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/10.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/11.jpg',
        originalAlt: "ремонт гостиной",
    },
    {
        original: '/images/slider/livingRoom/12.jpg',
        originalAlt: "ремонт гостиной",
    },
]

export const beforeAfter = [
    {
        original: '/images/slider/beforeafter/1.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/beforeafter/2.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/beforeafter/3.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/beforeafter/4.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/beforeafter/5.jpg',
        originalAlt: "до и после ремонта",
    },
]

export const koridor = [
    {
        original: '/images/slider/koridor/1.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/koridor/2.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/koridor/3.jpg',
        originalAlt: "до и после ремонта",
    },
    {
        original: '/images/slider/koridor/4.jpg',
        originalAlt: "до и после ремонта",
    },
]

const AboutContent = () => {
    const router = useRouter()
    const data = ['внушительное портфолио', 'огромный профессиональный опыт', 'более 1000 довольных клиентов']
    const data2 = ['Выполним все ремонтные работы в строго оговоренные сроки', 'Предоставляем прозрачную смету работ и материалов', 'Даём рекомендации по подбору качественных материалов', 'В случае необходимости доставим самостоятельно на объект строительные материалы', 'Обеспечим объект всем комплексом необходимых работ «под ключ»', 'Слелаем фотоотчет о проделанной работе', 'Гарантируем качество на все виды работ']
    const data3 = ['выезд специалистов по замеру в удобное для клиента время', 'рассрочка', 'быстрый старт работ', 'соблюдение сроков ремонта строго по договору', 'доставка и подъем материала']
    const data4 = [
        'При заказе комплексного ремонта действует скидка 10%. Разовые работы и мелкий ремонт оцениваются дороже.',
    'На комплексный ремонт под ключ предоставляется гарантия - 15 месяцев.',
    'Закупка + доставка материала + на дорожные расходы = 10% от стоимости материала.',
    'При проведении ремонта в заселенной квартире - стоимость работы увеличивается на 15%.',
    'При проведении работ в помещении с высотой потолка от 4 м и выше - применяется коэффициент 1,2.',
    'Откосом считается часть стены, ограниченная углами, шириной до 600 мм.',
    'К расходным материалам относятся: валики, ванночки, кисти, вёдра, наждачная шкурка.',
    ]
    const list = useMemo(() => (
        data.map(el => <li key={el}>{el};</li>)
    ), [])

    const list2 = useMemo(() => (
        data2.map(el => <li key={el}>{el};</li>)
    ), [])

    const list3 = useMemo(() => (
        data3.map(el => <li key={el}>{el};</li>)
    ), [])

    const list4 = useMemo(() => (
        data4.map(el => <li key={el}>{el};</li>)
    ), [])
    const ref = useRef()

    if (router?.query?.block) {
        window.scrollTo( 0, ref.current.offsetTop - 50)
    }

    return (
        <AboutWrapper>
            <HeaderH1 style={{textAlign: 'left'}}>
                О НАС
            </HeaderH1>

            <section>
                <p>
                    <b>BuildFurVit</b> - команда профессионалов своего дела. Мы работаем на Запорожском рынке
                    ремонтно-строительных услуг более 15 лет.
                </p>
                За многие годы успешной работы у нас есть:
                <ul>
                    {list}
                </ul>
            </section>

            <section>
                <p>
                    <b>Почему именно мы</b>
                </p>
                <p>
                    Благодаря безупречному качеству, умеренным ценам и доверия клиентов мы с лёгкостью и безупречностью
                    справляемся с конкуренцией и являемся лидерами на рынке.
                    К работе подходим очень ответственно.
                </p>
                <ul>
                    {list2}
                </ul>
            </section>

            <section>
                <p>
                    <b>НАШИ ВАКАНСИИ</b>
                </p>
                <p>
                    Мы постоянно растём и ищем новых амбициозных и опытных сотрудников, которые готовы учиться и могут
                    чему-то научить нас. Все вакансии актуальны, присылайте свои резюме на
                    почту <b><a href="buildfurvit@gmail.com">buildfurvit@gmail.com</a></b> или свяжитесь с нами по телефону, мы всегда
                    найдем место для хорошего сотрудника!
                </p>

                <p>
                    А так же если вы не имеете опыта работы, мы можем подобрать вам хорошего наставника с которым вы
                    пройдёте обучение и небольшую стажировку. После окончания вы автоматически становитесь нашим
                    сотрудником.
                </p>
            </section>

            <section ref={ref}>
                <p>
                    <b>Дополнительная информация</b>
                </p>
                <ul>
                    {list4}
                </ul>
            </section>

            <section>
                <p>
                    <b>ПРИЯТНЫЕ ПЛЮСЫ</b>
                </p>
                <p>
                    <b>BuildFurVit</b> занимает одно из лидирующих мест на рынке ремонта Запорожья и Запорожской
                    области!
                    Мы предлагаем качественный ремонт квартир по выгодным ценам в самые кратчайшие сроки. Наши клиенты
                    знают, что с ними на связи всегда: специалисты по замеру, сантехники, электрики, инженеры и другие
                    специалисты.
                </p>
                У нас есть все для взаимовыгодного сотрудничества:
                <ul>
                    {list3}
                </ul>
            </section>

            <section>
                <p>
                    <b>ПРИМЕРЫ НАШИХ РАБОТ - КУХНИ</b>
                </p>
                <Slider images={kitchen}/>
            </section>
            <section>
                <p>
                    <b>ПРИМЕРЫ НАШИХ РАБОТ - СПАЛЬНИ</b>
                </p>
                <Slider images={badRooms}/>
            </section>
            <section>
                <p>
                    <b>ПРИМЕРЫ НАШИХ РАБОТ - ГОСТИНЫЕ</b>
                </p>
                <Slider images={rooms}/>
            </section>

            <section>
                <p>
                    <b>ПРИМЕРЫ НАШИХ РАБОТ - ДО И ПОСЛЕ РЕМОНТА</b>
                </p>
                <Slider images={beforeAfter}/>
            </section>

            <section>
                <FlexSlider>
                    <div>
                        <p>
                            <b>ПРИМЕРЫ НАШИХ РАБОТ - КОРИДОР</b>
                        </p>
                        <Slider images={koridor} className="custom-gallery phone"/>
                    </div>
                    <div>
                        <p>
                            <b>ПРИМЕРЫ НАШИХ РАБОТ - </b>
                        </p>
                        <Slider images={koridor} className="custom-gallery phone"/>
                    </div>
                </FlexSlider>
            </section>
        </AboutWrapper>
    )
}

export default AboutContent