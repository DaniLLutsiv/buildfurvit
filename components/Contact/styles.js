import styled from 'styled-components'

export const ContactWrapper = styled.div`
  margin: 100px 0;
  padding: 25px;
  border: 1px solid #dddddd;
  display: flex;
  align-items: center;
  justify-content: space-between;
  
  & > img {
    margin-right: 50px;
    width: 300px;
    height: 300px;
  }
  
  @media (max-width: 768px){
    flex-direction: column;

    & > img {
      margin-right: 0;
      margin-bottom: 50px;
    }
  }

  @media (max-width: 480px){
    & > img {
      width: 100%;
      height: 100%;
    }
  }
`

export const ContactInfo = styled.div`
  font-size: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  
  & > div {
    display: flex;
    align-items: center;
    margin-bottom: 20px;
    min-height: 35px;
  }
  
  & > div > svg {
    margin-right: 10px;
    width: 30px;
    height: 30px;
  }
  
  & a {
    &:hover {
      text-decoration: underline;
    }
  }
`

export const InfoName = styled.div`
  font-size: 32px;
  font-weight: 700;
  margin-top: 0 !important;
  
  @media(max-width: 768px){
    font-size: 28px;
  }

  @media(max-width: 480px){
    font-size: 24px;
  }
`

export const Phone = styled.a`
  cursor: pointer;
  margin-bottom: 3px;
  display: flex;
  flex-direction: column;
`