import {ContactInfo, ContactWrapper, InfoName, Phone} from "./styles";
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';
import MailIcon from '@material-ui/icons/Mail';
import InstagramIcon from '@material-ui/icons/Instagram';
import {config} from "../../config";

const Contact = () => {
    return (
        <ContactWrapper>
            <img
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXqVOrqDGDYnd2CaWJzVWnDi75ZZzOQuH3UoTQddunEcBVND450sTSfLrZh9fvdbfw_eY&usqp=CAU"
                alt="location"/>
            <ContactInfo>
                <InfoName>
                    Контактная информация
                </InfoName>
                <div>
                    <LocationOnIcon/> г. Запорожье
                </div>
                <div>
                    <PhoneIcon/>
                    <div>
                        <Phone href={`tel:${config.PHONE_1}`}>{config.PHONE_1_STYLE}</Phone>
                        <Phone href={`tel:${config.PHONE_2}`}>{config.PHONE_2_STYLE}</Phone>
                    </div>
                </div>
                <div>
                    <MailIcon/>
                    <div>
                        <a href="https://buildfurvit@gmail.com">BuildFurVit@gmail.com</a>
                    </div>
                </div>

                <div>
                    <InstagramIcon/>
                    <div>
                        <a href="https://www.instagram.com/_buildfurvit_zp/" target="_blank">buildfurvit_zp</a>
                    </div>
                </div>
            </ContactInfo>
        </ContactWrapper>
    )
}

export default Contact